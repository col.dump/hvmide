/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hacker.hvm;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.View;
import java.awt.*;
import java.util.LinkedList;

public class JProgramTextArea extends JTextArea {

    Highlighter.HighlightPainter instructionMarkerPainter = new InstructionHighlightPainter(
            Color.getHSBColor(0.0f, 0.5f, 0.5f), Color.getHSBColor(0.0f, 0.1f, 1.0f));
    Highlighter.HighlightPainter targetPainter = new UnderlineHighlightPainter(
            Color.getHSBColor(0.6f, 0.5f, 0.5f), Color.getHSBColor(0.6f, 0.1f, 1.0f));
    Highlighter.HighlightPainter selectionPainter = new DefaultHighlighter.DefaultHighlightPainter(null);

    static class UnderlineHighlightPainter extends DefaultHighlighter.DefaultHighlightPainter {

        Color fg;

        public UnderlineHighlightPainter(Color fg, Color bg) {
            super(bg);
            this.fg = fg;
        }

        @Override
        public Shape paintLayer(Graphics g, int offs0, int offs1, Shape bounds, JTextComponent c, View view) {
            g.setColor(getColor());
            if (offs0 == view.getStartOffset() && offs1 == view.getEndOffset()) {
                // Contained in view, can just use bounds.
                return paintHighlight(g, bounds, fg);
            } else
            // Should only render part of View.
            {
                try {
                    Shape shape = view.modelToView(offs0, Position.Bias.Forward, offs1, Position.Bias.Backward, bounds);
                    Rectangle r = (shape instanceof Rectangle) ? (Rectangle) shape:shape.getBounds();
                    g.fillRect(r.x, r.y, r.width - 1, r.height - 1);
                    g.setColor(fg);
                    g.drawLine(r.x, r.y + r.height - 1, r.x + r.width - 1, r.y + r.height - 1);
                    return r;
                } catch (BadLocationException ignored) {
                }
            }
            return null;
        }

        static Shape paintHighlight(Graphics g, Shape bounds, Color fg) {
            Rectangle alloc;
            if (bounds instanceof Rectangle) {
                alloc = (Rectangle) bounds;
            } else {
                alloc = bounds.getBounds();
            }
            g.fillRect(alloc.x, alloc.y, alloc.width - 1, alloc.height - 1);
            g.setColor(fg);
            g.drawLine(alloc.x, alloc.y + alloc.height, alloc.x + alloc.width - 1, alloc.y + alloc.height - 1);
            return alloc;
        }

    }

    static class InstructionHighlightPainter extends DefaultHighlighter.DefaultHighlightPainter {

        Color fg;
        int[] markx, marky;

        public InstructionHighlightPainter(Color fg, Color bg) {
            super(bg);
            this.fg = fg;
            markx = new int[3];
            marky = new int[3];
        }

        @Override
        public Shape paintLayer(Graphics g, int offs0, int offs1, Shape bounds, JTextComponent c, View view) {
            g.setColor(getColor());
            if (offs0 == view.getStartOffset() && offs1 == view.getEndOffset()) {
                // Contained in view, can just use bounds.
                return UnderlineHighlightPainter.paintHighlight(g, bounds, fg);
            } else
            // Should only render part of View.
            {
                try {
                    Shape shape = view.modelToView(offs0, Position.Bias.Forward, offs1, Position.Bias.Backward, bounds);
                    Rectangle r = (shape instanceof Rectangle) ? (Rectangle) shape:shape.getBounds();
                    if (r.width % 2 == 1) {
                        r.width--;
                    } else {
                        r.width -= 2;
                    }
                    g.fillRect(r.x, r.y, r.width, r.height - 1);
                    g.setColor(fg);
                    int right = r.x + r.width, mid = r.x + r.width / 2, bottom = r.y + r.height - 1;
                    markx[0] = r.x;
                    marky[0] = r.y;
                    markx[1] = right;
                    marky[1] = r.y;
                    markx[2] = mid;
                    marky[2] = r.y + r.width / 2;
                    g.fillPolygon(markx, marky, 3);
                    g.drawLine(r.x, bottom, right, bottom);
                    return r;
                } catch (BadLocationException ignored) {
                }
            }
            return null;
        }

    }

    public void setHighlights(int from, int to, int instr) {
        Highlighter h = getHighlighter();
        h.removeAllHighlights();
        if (getText().isEmpty()) return;
        VirtualMachineModel vm = (VirtualMachineModel) getDocument();
        instr = vm.getDocumentPosition(instr);
        try {
            if (to > from) {
                LinkedList<VirtualMachineModel.CodeSegment> comments = vm.getCommentPositions();
                int f = from, t = to; // f<=t always (other cases: return above)
                for (VirtualMachineModel.CodeSegment c : comments) {
                    int s = c.getStart(), e = c.getEnd();
                    if (f < instr && instr < s) { // avoid hiding the instruction marker
                        h.addHighlight(f, instr, targetPainter);
                        f = instr + 1;
                    }
                    if (s <= f) { // comment before target area start -> just move both markers
                        f += e - s;
                    } else { // target area start before comment -> mark target up to comment
                        if (t < s) break; // target area ended before this comment
                        h.addHighlight(f, s, targetPainter);
                        f = e;
                    }
                    t += e - s;
                }
                if (f <= instr && t > instr) {
                    if (f < instr) h.addHighlight(f, instr, targetPainter);
                    f = instr + 1;
                }
                if (f < t) h.addHighlight(f, t, targetPainter);
            }
            h.addHighlight(instr, instr + 1, instructionMarkerPainter);
            // now restore selection
            int selstart = getSelectionStart(), selend = getSelectionEnd();
            if (selstart < selend) h.addHighlight(selstart, selend, selectionPainter);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

}
