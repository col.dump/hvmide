/*
 * VirtualMachineFrameView.java
 */

package org.hacker.hvm;

import org.jdesktop.application.Action;
import org.jdesktop.application.ApplicationActionMap;
import org.jdesktop.application.FrameView;
import org.jdesktop.application.SingleFrameApplication;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

// TODO: add status text

/**
 * The application's main frame.
 */
public class VirtualMachineFrameView extends FrameView {
    final String[] vmActions = {
            "reRunProgram", "continueProgram", "runToProgram",
            "resetVirtualMachine", "stepProgram", "stepOverProgram", "stepOutProgram",
            "goToCursor", "loadAsciiStringIntoMemory", "loadNumberSequenceIntoMemory",
            "runCycleNumProgram"
    };
    final byte MAJOR_VERSION = 1;
    final byte MINOR_VERSION = 0;
    final byte PATCHLEVEL = 0;
    VirtualMachinePanel activePanelBefore, activePanel;
    ApplicationActionMap myActionMap;
    VirtualMachineQuickReferenceDialog quickRef = null;
    File currentFile = null;
    PropertyChangeListener actionEnableDisableListener = new PropertyChangeListener() {
        public void propertyChange(PropertyChangeEvent evt) {
            javax.swing.Action undo = myActionMap.get("undoAction");
            javax.swing.Action redo = myActionMap.get("redoAction");
            undo.setEnabled(activePanel != null && activePanel.getModel().isCanUndo());
            redo.setEnabled(activePanel != null && activePanel.getModel().isCanRedo());
            if (activePanelBefore == null && activePanel != null) {
                for (String a : vmActions) myActionMap.get(a).setEnabled(true);
            } else if (activePanelBefore != null && activePanel == null) {
                for (String a : vmActions) myActionMap.get(a).setEnabled(false);
            }
        }
    };
    private int vmCount = 0;
    private javax.swing.JLabel statusLabel;
    private javax.swing.JTabbedPane tabbedPane;
    private JDialog aboutBox;

    public VirtualMachineFrameView(SingleFrameApplication app) {
        super(app);
        myActionMap = this.getContext().getActionMap(VirtualMachineFrameView.class, this);
        initComponents();
        activePanel = null;
        setStatusText(null);
    }

    @Action
    public void showAboutBox() {
        if (aboutBox == null) {
            JFrame mainFrame = VirtualMachineApp.getApplication().getMainFrame();
            aboutBox = new VirtualMachineAboutBox(mainFrame);
            aboutBox.setLocationRelativeTo(mainFrame);
        }
        VirtualMachineApp.getApplication().show(aboutBox);
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     */
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        javax.swing.JMenuBar menuBar = new javax.swing.JMenuBar();
        javax.swing.JMenu fileMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem newMenuItem = new javax.swing.JMenuItem();
        javax.swing.JMenuItem openMenuItem = new javax.swing.JMenuItem();
        javax.swing.JMenuItem saveMenuItem = new javax.swing.JMenuItem();
        javax.swing.JMenuItem jMenuItem2 = new javax.swing.JMenuItem();
        javax.swing.JMenuItem jMenuItem1 = new javax.swing.JMenuItem();
        javax.swing.JSeparator jSeparator5 = new javax.swing.JSeparator();
        javax.swing.JMenuItem propertiesMenuItem = new javax.swing.JMenuItem();
        javax.swing.JSeparator jSeparator3 = new javax.swing.JSeparator();
        javax.swing.JMenuItem exitMenuItem = new javax.swing.JMenuItem();
        javax.swing.JMenu editMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem undoMenuItem = new javax.swing.JMenuItem();
        javax.swing.JMenuItem redoMenuItem = new javax.swing.JMenuItem();
        javax.swing.JSeparator jSeparator4 = new javax.swing.JSeparator();
        javax.swing.JMenuItem cutMenuItem = new javax.swing.JMenuItem();
        javax.swing.JMenuItem copyMenuItem = new javax.swing.JMenuItem();
        javax.swing.JMenuItem pasteMenuItem = new javax.swing.JMenuItem();
        javax.swing.JMenu runMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem restartMenuItem = new javax.swing.JMenuItem();
        javax.swing.JMenuItem runMenuItem = new javax.swing.JMenuItem();
        javax.swing.JMenuItem runToCursorMenuItem = new javax.swing.JMenuItem();
        javax.swing.JMenuItem jMenuItem4 = new javax.swing.JMenuItem();
        javax.swing.JMenuItem resetMenuItem = new javax.swing.JMenuItem();
        javax.swing.JSeparator runSeparatorA = new javax.swing.JSeparator();
        javax.swing.JMenuItem stepMenuItem = new javax.swing.JMenuItem();
        javax.swing.JMenuItem stepOverMenuItem = new javax.swing.JMenuItem();
        javax.swing.JMenuItem stepOutMenuItem = new javax.swing.JMenuItem();
        javax.swing.JSeparator runSeparatorB = new javax.swing.JSeparator();
        javax.swing.JMenuItem goToMenuItem = new javax.swing.JMenuItem();
        javax.swing.JMenu memoryMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem asciiMemoryMenuItem = new javax.swing.JMenuItem();
        javax.swing.JMenuItem jMenuItem3 = new javax.swing.JMenuItem();
        javax.swing.JMenu helpMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem quickrefMenuItem = new javax.swing.JMenuItem();
        javax.swing.JMenuItem aboutMenuItem = new javax.swing.JMenuItem();
        javax.swing.JToolBar toolBar = new javax.swing.JToolBar();
        javax.swing.JButton newToolBarButton = new javax.swing.JButton();
        javax.swing.JButton openButton = new javax.swing.JButton();
        javax.swing.JButton saveButton = new javax.swing.JButton();
        javax.swing.JToolBar.Separator jSeparator1 = new javax.swing.JToolBar.Separator();
        javax.swing.JButton runButton = new javax.swing.JButton();
        javax.swing.JButton continueButton = new javax.swing.JButton();
        javax.swing.JButton resetButton = new javax.swing.JButton();
        javax.swing.JToolBar.Separator jSeparator2 = new javax.swing.JToolBar.Separator();
        javax.swing.JButton runToButton = new javax.swing.JButton();
        javax.swing.JButton stepButton = new javax.swing.JButton();
        javax.swing.JButton stepOverButton = new javax.swing.JButton();
        javax.swing.JButton stepOutButton = new javax.swing.JButton();
        javax.swing.JButton goToButton = new javax.swing.JButton();
        javax.swing.JPanel statusPanel = new javax.swing.JPanel();
        javax.swing.JSeparator statusSeparator = new javax.swing.JSeparator();
        statusLabel = new javax.swing.JLabel();
        tabbedPane = new javax.swing.JTabbedPane();

        menuBar.setName("menuBar");

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(org.hacker.hvm.VirtualMachineApp.class).getContext()
                .getResourceMap(VirtualMachineFrameView.class);
        fileMenu.setText(resourceMap.getString("fileMenu.text"));
        fileMenu.setName("fileMenu");

        javax.swing.ActionMap actionMap = org.jdesktop.application.Application.getInstance(org.hacker.hvm.VirtualMachineApp.class).getContext()
                .getActionMap(VirtualMachineFrameView.class, this);
        newMenuItem.setAction(actionMap.get("newVirtualMachine"));
        newMenuItem.setName("newMenuItem");
        fileMenu.add(newMenuItem);

        openMenuItem.setAction(actionMap.get("openFile"));
        openMenuItem.setName("openMenuItem");
        fileMenu.add(openMenuItem);

        saveMenuItem.setAction(actionMap.get("saveFile"));
        saveMenuItem.setName("saveMenuItem");
        fileMenu.add(saveMenuItem);

        jMenuItem2.setAction(actionMap.get("saveAs"));
        jMenuItem2.setName("jMenuItem2");
        fileMenu.add(jMenuItem2);

        jMenuItem1.setAction(actionMap.get("close"));
        jMenuItem1.setName("jMenuItem1");
        fileMenu.add(jMenuItem1);

        jSeparator5.setName("jSeparator5");
        fileMenu.add(jSeparator5);

        propertiesMenuItem.setAction(actionMap.get("vmPropertiesDialog"));
        propertiesMenuItem.setName("propertiesMenuItem");
        fileMenu.add(propertiesMenuItem);

        jSeparator3.setName("jSeparator3");
        fileMenu.add(jSeparator3);

        exitMenuItem.setAction(actionMap.get("quit"));
        exitMenuItem.setName("exitMenuItem");
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        editMenu.setText(resourceMap.getString("editMenu.text"));
        editMenu.setName("editMenu");

        undoMenuItem.setAction(actionMap.get("undoAction"));
        undoMenuItem.setName("undoMenuItem");
        editMenu.add(undoMenuItem);

        redoMenuItem.setAction(actionMap.get("redoAction"));
        redoMenuItem.setName("redoMenuItem");
        editMenu.add(redoMenuItem);

        jSeparator4.setName("jSeparator4");
        editMenu.add(jSeparator4);

        cutMenuItem.setAction(actionMap.get("cut"));
        cutMenuItem.setName("cutMenuItem");
        editMenu.add(cutMenuItem);

        copyMenuItem.setAction(actionMap.get("copy"));
        copyMenuItem.setName("copyMenuItem");
        editMenu.add(copyMenuItem);

        pasteMenuItem.setAction(actionMap.get("paste"));
        pasteMenuItem.setName("pasteMenuItem");
        editMenu.add(pasteMenuItem);

        menuBar.add(editMenu);

        runMenu.setText(resourceMap.getString("runMenu.text"));
        runMenu.setName("runMenu");

        restartMenuItem.setAction(actionMap.get("reRunProgram"));
        restartMenuItem.setText(resourceMap.getString("restartMenuItem.text"));
        restartMenuItem.setName("restartMenuItem");
        runMenu.add(restartMenuItem);

        runMenuItem.setAction(actionMap.get("continueProgram"));
        runMenuItem.setText(resourceMap.getString("runMenuItem.text"));
        runMenuItem.setName("runMenuItem");
        runMenu.add(runMenuItem);

        runToCursorMenuItem.setAction(actionMap.get("runToProgram"));
        runToCursorMenuItem.setText(resourceMap.getString("runToCursorMenuItem.text"));
        runToCursorMenuItem.setName("runToCursorMenuItem");
        runMenu.add(runToCursorMenuItem);

        jMenuItem4.setAction(actionMap.get("runCycleNumProgram"));
        jMenuItem4.setName("jMenuItem4");
        runMenu.add(jMenuItem4);

        resetMenuItem.setAction(actionMap.get("resetVirtualMachine"));
        resetMenuItem.setText(resourceMap.getString("resetMenuItem.text"));
        resetMenuItem.setName("resetMenuItem");
        runMenu.add(resetMenuItem);

        runSeparatorA.setName("runSeparatorA");
        runMenu.add(runSeparatorA);

        stepMenuItem.setAction(actionMap.get("stepProgram"));
        stepMenuItem.setText(resourceMap.getString("stepMenuItem.text"));
        stepMenuItem.setName("stepMenuItem");
        runMenu.add(stepMenuItem);

        stepOverMenuItem.setAction(actionMap.get("stepOverProgram"));
        stepOverMenuItem.setText(resourceMap.getString("stepOverMenuItem.text"));
        stepOverMenuItem.setName("stepOverMenuItem");
        runMenu.add(stepOverMenuItem);

        stepOutMenuItem.setAction(actionMap.get("stepOutProgram"));
        stepOutMenuItem.setText(resourceMap.getString("stepOutMenuItem.text"));
        stepOutMenuItem.setName("stepOutMenuItem");
        runMenu.add(stepOutMenuItem);

        runSeparatorB.setName("runSeparatorB");
        runMenu.add(runSeparatorB);

        goToMenuItem.setAction(actionMap.get("goToCursor"));
        goToMenuItem.setName("goToMenuItem");
        runMenu.add(goToMenuItem);

        menuBar.add(runMenu);

        memoryMenu.setText(resourceMap.getString("memoryMenu.text"));
        memoryMenu.setName("memoryMenu");

        asciiMemoryMenuItem.setAction(actionMap.get("loadAsciiStringIntoMemory"));
        asciiMemoryMenuItem.setName("asciiMemoryMenuItem");
        memoryMenu.add(asciiMemoryMenuItem);

        jMenuItem3.setAction(actionMap.get("loadNumberSequenceIntoMemory"));
        jMenuItem3.setName("jMenuItem3");
        memoryMenu.add(jMenuItem3);

        menuBar.add(memoryMenu);

        helpMenu.setText(resourceMap.getString("helpMenu.text"));
        helpMenu.setName("helpMenu");

        quickrefMenuItem.setAction(actionMap.get("showQuickReference"));
        quickrefMenuItem.setName("quickrefMenuItem");
        helpMenu.add(quickrefMenuItem);

        aboutMenuItem.setAction(actionMap.get("showAboutBox"));
        aboutMenuItem.setName("aboutMenuItem");
        helpMenu.add(aboutMenuItem);

        menuBar.add(helpMenu);

        toolBar.setRollover(true);
        toolBar.setName("toolBar");

        newToolBarButton.setAction(actionMap.get("newVirtualMachine"));
        newToolBarButton.setFocusable(false);
        newToolBarButton.setHideActionText(true);
        newToolBarButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        newToolBarButton.setName("newToolBarButton");
        newToolBarButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(newToolBarButton);

        openButton.setAction(actionMap.get("openFile"));
        openButton.setFocusable(false);
        openButton.setHideActionText(true);
        openButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        openButton.setName("openButton");
        openButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(openButton);

        saveButton.setAction(actionMap.get("saveFile"));
        saveButton.setFocusable(false);
        saveButton.setHideActionText(true);
        saveButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        saveButton.setName("saveButton");
        saveButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(saveButton);

        jSeparator1.setName("jSeparator1");
        toolBar.add(jSeparator1);

        runButton.setAction(actionMap.get("reRunProgram"));
        runButton.setFocusable(false);
        runButton.setHideActionText(true);
        runButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        runButton.setName("runButton");
        runButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(runButton);

        continueButton.setAction(actionMap.get("continueProgram"));
        continueButton.setFocusable(false);
        continueButton.setHideActionText(true);
        continueButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        continueButton.setName("continueButton");
        continueButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(continueButton);

        resetButton.setAction(actionMap.get("resetVirtualMachine"));
        resetButton.setFocusable(false);
        resetButton.setHideActionText(true);
        resetButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        resetButton.setName("resetButton");
        resetButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(resetButton);

        jSeparator2.setName("jSeparator2");
        toolBar.add(jSeparator2);

        runToButton.setAction(actionMap.get("runToProgram"));
        runToButton.setFocusable(false);
        runToButton.setHideActionText(true);
        runToButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        runToButton.setName("runToButton");
        runToButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(runToButton);

        stepButton.setAction(actionMap.get("stepProgram"));
        stepButton.setFocusable(false);
        stepButton.setHideActionText(true);
        stepButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        stepButton.setName("stepButton");
        stepButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(stepButton);

        stepOverButton.setAction(actionMap.get("stepOverProgram"));
        stepOverButton.setFocusable(false);
        stepOverButton.setHideActionText(true);
        stepOverButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        stepOverButton.setName("stepOverButton");
        stepOverButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(stepOverButton);

        stepOutButton.setAction(actionMap.get("stepOutProgram"));
        stepOutButton.setFocusable(false);
        stepOutButton.setHideActionText(true);
        stepOutButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        stepOutButton.setName("stepOutButton");
        stepOutButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(stepOutButton);

        goToButton.setAction(actionMap.get("goToCursor"));
        goToButton.setFocusable(false);
        goToButton.setHideActionText(true);
        goToButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        goToButton.setName("goToButton");
        goToButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(goToButton);

        statusPanel.setName("statusPanel");
        statusPanel.setLayout(new java.awt.GridBagLayout());

        statusSeparator.setName("statusSeparator");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        statusPanel.add(statusSeparator, gridBagConstraints);

        statusLabel.setText(resourceMap.getString("statusLabel.text"));
        statusLabel.setName("statusLabel");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 3, 2, 3);
        statusPanel.add(statusLabel, gridBagConstraints);

        tabbedPane.setBackground(resourceMap.getColor("tabbedPane.background"));
        tabbedPane.setTabPlacement(javax.swing.JTabbedPane.BOTTOM);
        tabbedPane.setName("tabbedPane");
        tabbedPane.addChangeListener(this::tabbedPaneStateChanged);

        setComponent(tabbedPane);
        setMenuBar(menuBar);
        setStatusBar(statusPanel);
        setToolBar(toolBar);
    }

    private void tabbedPaneStateChanged(javax.swing.event.ChangeEvent evt) {
        activePanelBefore = activePanel;
        if (activePanel != null) activePanel.getModel().removePropertyChangeListener(actionEnableDisableListener);
        activePanel = (VirtualMachinePanel) tabbedPane.getSelectedComponent();
        if (activePanel != null) activePanel.getModel().addPropertyChangeListener(actionEnableDisableListener);
        actionEnableDisableListener.propertyChange(null);
    }

    public void setStatusText(String t) {
        if (t != null && !t.isEmpty()) {
            statusLabel.setText(t);
        } else {
            statusLabel.setText(" ");
        }
    }

    @Action
    public void reRunProgram() {
        if (activePanel == null) return;
        activePanel.getModel().reset();
        activePanel.getModel().run();
        activePanel.updateCaretPosition();
        activePanel.updateOutput();
    }

    @Action
    public void continueProgram() {
        if (activePanel == null) return;
        activePanel.getModel().run();
        activePanel.updateCaretPosition();
        activePanel.updateOutput();
    }

    @Action
    public void runToProgram() {
        if (activePanel == null) return;
        VirtualMachineModel m = activePanel.getModel();
        m.runTo(activePanel.getCaretPosition());
        activePanel.updateCaretPosition();
        activePanel.updateOutput();
    }

    @Action
    public void resetVirtualMachine() {
        if (activePanel == null) return;
        activePanel.getModel().reset();
        activePanel.updateCaretPosition();
        activePanel.updateOutput();
    }

    @Action
    public void stepProgram() {
        if (activePanel == null) return;
        activePanel.getModel().step();
        activePanel.updateCaretPosition();
        activePanel.updateOutput();
    }

    @Action
    public void stepOverProgram() {
        if (activePanel == null) return;
        activePanel.getModel().stepOver();
        activePanel.updateCaretPosition();
        activePanel.updateOutput();
    }

    @Action
    public void stepOutProgram() {
        if (activePanel == null) return;
        activePanel.getModel().stepOut();
        activePanel.updateCaretPosition();
        activePanel.updateOutput();
    }

    @Action
    public void goToCursor() {
        if (activePanel == null) return;
        VirtualMachineModel m = activePanel.getModel();
        m.goTo(activePanel.getCaretPosition());
        activePanel.updateCaretPosition();
    }

    @Action
    public void undoAction() {
        if (activePanel == null) return;
        activePanel.undo();
        activePanel.updateCaretPosition();
    }

    @Action
    public void redoAction() {
        if (activePanel == null) return;
        activePanel.redo();
        activePanel.updateCaretPosition();
    }

    @Action
    public void newVirtualMachine() {
        VirtualMachinePanel v = new VirtualMachinePanel(this);
        String t = "VM #" + (++vmCount);
        tabbedPane.add(t, v);
        tabbedPane.setTabComponentAt(tabbedPane.indexOfComponent(v), new JRenameableTab(tabbedPane, v));
        tabbedPane.setSelectedComponent(v);
    }

    @Action
    public void openFile() {
        StringBuilder unsaved = new StringBuilder();
        for (int i = 0; i < tabbedPane.getTabCount(); i++) {
            if (((VirtualMachinePanel) tabbedPane.getComponentAt(i)).getModel().hasUnsavedChanges()) {
                unsaved.append("- ").append(tabbedPane.getTitleAt(i)).append("\n");
            }
        }
        if (unsaved.length() > 0 && JOptionPane.showConfirmDialog(this.getComponent(), "The following virtual machines have unsaved changes:\n" +
                unsaved + "\nDo you really want to abandon these changes?", "Confirm open", JOptionPane.OK_CANCEL_OPTION) != JOptionPane.OK_OPTION) {
            return;
        }
        JFileChooser c = new JFileChooser();
        File file;
        setHvmFileFilter(c);
        do {
            if (c.showOpenDialog(this.getComponent()) != JFileChooser.APPROVE_OPTION) {
                return;
            }
            file = c.getSelectedFile();
            String filename = c.getSelectedFile().getName();
            if (!filename.toLowerCase().endsWith(".hvm")) {
                file = new File(file.getParent(), filename + ".hvm");
            }
            if (!file.exists()) {
                JOptionPane.showMessageDialog(this.getComponent(), file.getName() + " does not exists.", "File missing", JOptionPane.INFORMATION_MESSAGE);
            }
        } while (!file.exists());
        try {
            FileInputStream stream = new FileInputStream(file);
            ObjectInputStream in = new ObjectInputStream(stream);
            byte[] header = new byte[7];
            //noinspection ResultOfMethodCallIgnored
            in.read(header);
            if (!new String(header).equals("COLDUMP")) {
                throw new IOException("Invalid file format");
            }
            int majorVersion = in.readByte(), minorVersion = in.readByte(), patchlevel = in.readByte();
            if (majorVersion > 1 && minorVersion > 0 && patchlevel > 0) {
                throw new IOException("Invalid file version " + majorVersion + "." + minorVersion + "." + patchlevel);
            }
            int n = in.read();
            tabbedPane.removeAll();
            for (int i = 0; i < n; i++) {
                VirtualMachinePanel p = new VirtualMachinePanel(this);
                String t = (String) in.readObject();
                p.getModel().readObject(in);
                tabbedPane.add(p);
                tabbedPane.setTitleAt(i, t);
                p.getModel().setSaved();
            }
            int s = in.read();
            tabbedPane.setSelectedIndex(s);
            in.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        currentFile = file;
        String t = this.getApplication().getContext().getResourceMap().getString("Application.title");
        this.getFrame().setTitle(t + " [" + currentFile.getName() + "]");
    }

    @Action
    public void saveAs() {
        JFileChooser c = new JFileChooser();
        int answer = 0;
        setHvmFileFilter(c);
        do {
            if (c.showSaveDialog(this.getComponent()) != JFileChooser.APPROVE_OPTION) {
                return;
            }
            currentFile = c.getSelectedFile();
            String filename = c.getSelectedFile().getName();
            if (!filename.toLowerCase().endsWith(".hvm")) {
                currentFile = new File(currentFile.getParent(), filename + ".hvm");
            }
            if (currentFile.exists()) {
                answer = JOptionPane.showConfirmDialog(this.getComponent(), currentFile.getName() + " already exists. Overwrite?", "File exists",
                        JOptionPane.OK_CANCEL_OPTION);
            }
        } while (answer != 0);
        saveFile();
        String t = this.getApplication().getContext().getResourceMap().getString("Application.title");
        System.out.println(t);
        this.getFrame().setTitle(t + " [" + currentFile.getName() + "]");
    }

    private void setHvmFileFilter(JFileChooser c) {
        c.setFileFilter(new FileFilter() {
            public boolean accept(File f) {
                if (f.isDirectory()) return true;
                return f.getName().toLowerCase().endsWith(".hvm");
            }

            @Override
            public String getDescription() {
                return "HVM files";
            }
        });
        c.setAcceptAllFileFilterUsed(false);
    }

    @Action
    public void saveFile() {
        if (currentFile == null) {
            saveAs();
        } else {
            try {
                FileOutputStream stream = new FileOutputStream(currentFile);
                ObjectOutputStream out = new ObjectOutputStream(stream);
                out.write("COLDUMP".getBytes());
                out.write(MAJOR_VERSION);
                out.write(MINOR_VERSION);
                out.write(PATCHLEVEL);
                int n = tabbedPane.getTabCount();
                out.write(n);
                for (int i = 0; i < n; i++) {
                    VirtualMachinePanel p = (VirtualMachinePanel) tabbedPane.getComponentAt(i);
                    out.writeObject(tabbedPane.getTitleAt(i));
                    p.getModel().writeObject(out);
                }
                out.write(tabbedPane.getSelectedIndex());
                out.flush();
                out.close();
                stream.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        setStatusText(currentFile.getName() + " saved.");
        for (int i = 0; i < tabbedPane.getTabCount(); i++) {
            ((VirtualMachinePanel) tabbedPane.getComponentAt(i)).getModel().setSaved();
        }
    }

    @Action
    public void cut() {
    }

    @Action
    public void copy() {
    }

    @Action
    public void paste() {
    }

    @Action
    public void showQuickReference() {
        if (quickRef == null) {
            quickRef = new VirtualMachineQuickReferenceDialog(this.getFrame(), false);
            quickRef.setVisible(true);
        } else {
            quickRef.setVisible(true);
        }
        this.getFrame().requestFocus();
        if (activePanel != null) activePanel.requestFocus();
    }

    @Action
    public void vmPropertiesDialog() {
        if (activePanel == null) return;
        VirtualMachinePropertiesDialog d = new VirtualMachinePropertiesDialog(this.getFrame(), activePanel);
        d.setVisible(true);
    }

    @Action
    public void close() {
        if (activePanel == null) return;
        if (activePanel.getModel().hasUnsavedChanges()) {
            if (JOptionPane.showConfirmDialog(this.getComponent(), "This virtual machine has unsaved changes.\n" +
                    "Do you really want to abandon these changes?", "Confirm close", JOptionPane.OK_CANCEL_OPTION) != JOptionPane.OK_OPTION) {
                return;
            }
        }
        int i = tabbedPane.indexOfComponent(activePanel);
        tabbedPane.remove(activePanel);
        int n = tabbedPane.getTabCount();
        if (n == 0) {
            activePanel = null;
        } else if (n <= i) {
            activePanel = (VirtualMachinePanel) tabbedPane.getComponentAt(n - i);
        } else {
            activePanel = (VirtualMachinePanel) tabbedPane.getComponentAt(i);
        }
        if (activePanel != null) tabbedPane.setSelectedComponent(activePanel);
    }

    @Action
    public void loadAsciiStringIntoMemory() {
        String s = JOptionPane.showInputDialog(this.getComponent(), """
                The following string will be transferred
                bytewise into memory and will be terminated
                with a zero value:""", "Load ASCI Characters", JOptionPane.WARNING_MESSAGE);
        if (s != null) {
            VirtualMachineModel vm = activePanel.getModel();
            vm.loadAsciiCharsIntoMemory(s);
            activePanel.updateOutput();
        }
    }

    @Action
    public void loadNumberSequenceIntoMemory() {
        String s = JOptionPane.showInputDialog(this.getComponent(), "Enter comma-separated memory values\n" +
                "(decimal, spaces are ignored):", "Load number sequence", JOptionPane.WARNING_MESSAGE);
        if (s != null) {
            VirtualMachineModel vm = activePanel.getModel();
            vm.loadNumberStringIntoMemory(s);
            activePanel.updateOutput();
        }
    }

    @Action
    public void runCycleNumProgram() {
        String s = JOptionPane.showInputDialog(this.getComponent(), "Enter the number of cycles to run:\n", "Run to cycle X",
                JOptionPane.WARNING_MESSAGE);
        if (s != null) {
            try {
                int n = Integer.parseInt(s);
                if (n <= 0) throw new NumberFormatException();
                VirtualMachineModel vm = activePanel.getModel();
                int l = vm.getCycleLimit();
                if (l < n) {
                    setStatusText("Raised cycle limit for '" +
                            tabbedPane.getTitleAt(tabbedPane.indexOfComponent(activePanel)) +
                            "' permanently to " + n + ".");
                }
                vm.setCycleLimit(n);
                vm.reset();
                vm.run();
                if (l > n) vm.setCycleLimit(n);
                activePanel.updateOutput();
            } catch (NumberFormatException ex) {
                setStatusText("'" + s + "' is not a valid positive decimal number.");
            }
        }
    }
}
