/*
 * VirtualMachineApp.java
 */

package org.hacker.hvm;

import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;

/**
 * The main class of the application.
 */
public class VirtualMachineApp extends SingleFrameApplication {

    /**
     * At startup create and show the main frame of the application.
     */
    @Override
    protected void startup() {
        VirtualMachineFrameView v = new VirtualMachineFrameView(this);
        v.newVirtualMachine();
        show(v);
    }

    /**
     * This method is to initialize the specified window by injecting resources.
     * Windows shown in our application come fully initialized from the GUI
     * builder, so this additional configuration is not needed.
     */
    @Override
    protected void configureWindow(java.awt.Window root) {
    }

    /**
     * A convenient static getter for the application instance.
     *
     * @return the instance of VirtualMachineApp
     */
    public static VirtualMachineApp getApplication() {
        return Application.getInstance(VirtualMachineApp.class);
    }

    /**
     * Main method launching the application.
     */
    public static void main(String[] args) {
        System.setProperty("awt.useSystemAAFontSettings", "on");
        System.setProperty("swing.aatext", "true");
        launch(VirtualMachineApp.class, args);
    }
}
