/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * VirtualMachinePropertiesDialog.java
 *
 * Created on 19.11.2008, 17:13:16
 */

package org.hacker.hvm;

import javax.swing.*;

public class VirtualMachinePropertiesDialog extends javax.swing.JDialog {
    VirtualMachineModel vm;
    private javax.swing.JTextField callstackTextField;
    private javax.swing.JTextField cyclesTextField;
    private javax.swing.JTextField memoryTextField;
    private javax.swing.JTextField stackTextField;

    /** Creates new form VirtualMachinePropertiesDialog */
    public VirtualMachinePropertiesDialog(java.awt.Frame parent, VirtualMachinePanel p) {
        super(parent, true);
        initComponents();
        vm = p.getModel();
        cyclesTextField.setText(Integer.toString(vm.getCycleLimit()));
        stackTextField.setText(Integer.toString(vm.getMaxStackSize()));
        callstackTextField.setText(Integer.toString(vm.getMaxCallStackSize()));
        memoryTextField.setText(Integer.toString(vm.getMemorySize()));
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     */
    private void initComponents() {

        JLabel jLabel1 = new JLabel();
        JLabel jLabel2 = new JLabel();
        JLabel jLabel3 = new JLabel();
        JLabel jLabel4 = new JLabel();
        callstackTextField = new javax.swing.JTextField();
        memoryTextField = new javax.swing.JTextField();
        stackTextField = new javax.swing.JTextField();
        cyclesTextField = new javax.swing.JTextField();
        JButton cancelButton = new JButton();
        JButton okButton = new JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(org.hacker.hvm.VirtualMachineApp.class).getContext()
                .getResourceMap(VirtualMachinePropertiesDialog.class);
        setTitle(resourceMap.getString("Form.title"));
        setName("Form");

        jLabel1.setText(resourceMap.getString("jLabel1.text"));
        jLabel1.setName("jLabel1");

        jLabel2.setText(resourceMap.getString("jLabel2.text"));
        jLabel2.setName("jLabel2");

        jLabel3.setText(resourceMap.getString("jLabel3.text"));
        jLabel3.setName("jLabel3");

        jLabel4.setText(resourceMap.getString("jLabel4.text"));
        jLabel4.setName("jLabel4");

        callstackTextField.setText(resourceMap.getString("callstackTextField.text"));
        callstackTextField.setName("callstackTextField");

        memoryTextField.setText(resourceMap.getString("memoryTextField.text"));
        memoryTextField.setName("memoryTextField");

        stackTextField.setText(resourceMap.getString("stackTextField.text"));
        stackTextField.setName("stackTextField");

        cyclesTextField.setText(resourceMap.getString("cyclesTextField.text"));
        cyclesTextField.setName("cyclesTextField");

        cancelButton.setText(resourceMap.getString("cancelButton.text"));
        cancelButton.setName("cancelButton");
        cancelButton.addActionListener(this::cancelButtonActionPerformed);

        okButton.setText(resourceMap.getString("okButton.text"));
        okButton.setName("okButton");
        okButton.addActionListener(this::okButtonActionPerformed);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(okButton)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(cancelButton))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabel4)
                                                        .addComponent(jLabel3)
                                                        .addComponent(jLabel2)
                                                        .addComponent(jLabel1))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(cyclesTextField)
                                                        .addComponent(stackTextField)
                                                        .addComponent(memoryTextField)
                                                        .addComponent(callstackTextField))))
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel1)
                                        .addComponent(cyclesTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel2)
                                        .addComponent(stackTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel3)
                                        .addComponent(memoryTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel4)
                                        .addComponent(callstackTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(cancelButton)
                                        .addComponent(okButton))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {
        setVisible(false);
    }

    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {
        int a, b, c, d;
        try {
            cyclesTextField.requestFocus();
            a = Integer.parseInt(cyclesTextField.getText());
            stackTextField.requestFocus();
            b = Integer.parseInt(stackTextField.getText());
            memoryTextField.requestFocus();
            c = Integer.parseInt(memoryTextField.getText());
            callstackTextField.requestFocus();
            d = Integer.parseInt(callstackTextField.getText());
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(this, "Invalid number format.", "Error", JOptionPane.WARNING_MESSAGE);
            return;
        }
        vm.setCycleLimit(a);
        vm.setMaxStackSize(b);
        vm.setMemorySize(c);
        vm.setMaxCallStackSize(d);
        vm.updateTables();
        setVisible(false);
    }
    // End of variables declaration

}
