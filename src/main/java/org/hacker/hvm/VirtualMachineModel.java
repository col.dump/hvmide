/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.hacker.hvm;

import org.hacker.hvm.VirtualMachine.VirtualMachineException;

import javax.swing.table.AbstractTableModel;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;
import javax.swing.undo.UndoManager;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


// TODO: public/protected/private access

public class VirtualMachineModel extends PlainDocument implements Serializable {

    VirtualMachine vm;
    VirtualMachine.VirtualMachineException lastException;
    AbstractTableModel stackModel, callStackModel, memoryModel;
    Pattern docToCodeRegex = Pattern.compile("#.*(\\n|\\z)|\\n+"); // see Pattern javadocs for regexp syntax
    Matcher docToCodeMatcher = docToCodeRegex.matcher("");
    boolean changed = false;

    public static class CodeSegment {
        private final int start;
        private final int end;

        public CodeSegment(int start, int end) {
            this.start = start;
            this.end = end;
        }

        public int getStart() {
            return start;
        }

        public int getEnd() {
            return end;
        }
    }

    LinkedList<CodeSegment> removals;
    UndoManager undoManager;

    public VirtualMachineModel() {
        vm = new VirtualMachine();
        undoManager = new UndoManager();
        addUndoableEditListener(undoManager);
        removals = new LinkedList<>();
    }

    public AbstractTableModel getStackTableModel() {
        if (stackModel == null) stackModel = new StackTableModel(vm);
        return stackModel;
    }

    public AbstractTableModel getMemoryTableModel() {
        if (memoryModel == null) memoryModel = new MemoryTableModel(vm);
        return memoryModel;
    }

    public AbstractTableModel getCallStackTableModel() {
        if (callStackModel == null) callStackModel = new CallStackTableModel(vm);
        return callStackModel;
    }

    @Override
    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
        super.insertString(offs, str, a);
        updateProgramCode();
        setCanUndo(undoManager.canUndo());
        changed = true;
    }

    @Override
    public void remove(int offs, int len) throws BadLocationException {
        super.remove(offs, len);
        updateProgramCode();
        setCanUndo(undoManager.canUndo());
        changed = true;
    }

    public char getCommandAt(int p) {
        return vm.getCommandAt(p);
    }

    public char[] getProgram() {
        return vm.getProgram();
    }

    public int getInstructionPointer() {
        return vm.getInstructionPointer();
    }

    public int getCycles() {
        return vm.getCycles();
    }

    public int getStackSize() {
        return vm.getStackSize();
    }

    public int getCycleLimit() {
        return vm.getCycleLimit();
    }

    public int getCallStackSize() {
        return vm.getCallStackSize();
    }

    public int getMemorySize() {
        return vm.getMemorySize();
    }

    public int getMaxStackSize() {
        return vm.getMaxStackSize();
    }

    public void setMaxStackSize(int s) {
        changed = true;
        vm.setMaxStackSize(s);
    }

    public int getMaxCallStackSize() {
        return vm.getMaxCallStackSize();
    }

    public void setMemorySize(int m) {
        changed = true;
        vm.setMemorySize(m);
    }

    public void setCycleLimit(int CycleLimit) {
        changed = true;
        vm.setCycleLimit(CycleLimit);
    }

    public void setMaxCallStackSize(int s) {
        changed = true;
        vm.setMaxCallStackSize(s);
    }

    private void updateProgramCode() {
        try {
            int l = getLength();
            docToCodeMatcher.reset(getText(0, l));
            removals.clear();
            while (docToCodeMatcher.find()) {
                removals.add(new CodeSegment(docToCodeMatcher.start(), docToCodeMatcher.end()));
            }
            String code = docToCodeMatcher.replaceAll("");
            vm.setProgram(code);
        } catch (Exception ex) {
            System.err.println("Error: " + ex);
        }
    }

    public int getCodePosition(int docPosition) {
        int d = docPosition;
        for (CodeSegment c : removals) {
            int s = c.getStart(), e = c.getEnd();
            if (s <= docPosition && e >= docPosition) {
                return d - (docPosition - s);
            }
            if (e < docPosition) d -= e - s;
        }
        return d;
    }

    public int getDocumentPosition(int codePosition) {
        int p = codePosition;
        for (CodeSegment c : removals) {
            if (c.getStart() <= p) {
                p += c.getEnd() - c.getStart();
            } else {
                break;
            }
        }
        return p;
    }

    public boolean isDocumentPositionInComment(int docPosition) {
        for (CodeSegment c : removals) {
            if (c.getStart() <= docPosition && c.getEnd() > docPosition) return true;
        }
        return false;
    }

    public LinkedList<CodeSegment> getCommentPositions() {
        return removals;
    }

    public void loadAsciiCharsIntoMemory(String s) {
        changed = true;
        lastException = null;
        try {
            vm.loadAsciiCharsIntoMemory(s);
        } catch (VirtualMachineException ex) {
            lastException = ex;
        }
        if (memoryModel != null) memoryModel.fireTableDataChanged();
    }

    public void loadNumberStringIntoMemory(String m) {
        changed = true;
        vm.initializeMemoryFromString(m);
        if (memoryModel != null) memoryModel.fireTableDataChanged();
    }

    public String getStatusMessage() {
        if (lastException == null) return null;
        return (lastException.getMessage() + " at instruction " + lastException.getInstructionPointer());
    }

    public String getOutput() {
        return vm.getOutput();
    }

    public boolean step() {
        changed = true;
        char c = vm.getNextCommand();
        try {
            if (c == 'v') {
                int s0 = vm.peek() + 1;
                vm.step();
                if (stackModel != null) {
                    stackModel.fireTableRowsDeleted(s0, s0);
                    stackModel.fireTableRowsInserted(0, 0);
                }
                return true;
            }
            int result = vm.step();
            if (c >= '0' && c <= '9' || c == '^') // push-type operations
            {
                if (stackModel != null) stackModel.fireTableRowsInserted(0, 0);
            }
            if (c == '<' || c == '^') // pop-push-type operations
            {
                if (stackModel != null) stackModel.fireTableRowsUpdated(0, 0);
            }
            if (c == 'd' || c == 'g' || c == 'p' || c == 'P') // pop-type-operations
            {
                if (stackModel != null) stackModel.fireTableRowsDeleted(0, 0);
            }
            if (c == '+' || c == '-' || c == '*' || c == '/' || c == ':') // pop-pop-push-type operations
            {
                if (stackModel != null) {
                    stackModel.fireTableRowsDeleted(0, 0);
                    stackModel.fireTableRowsUpdated(0, 0);
                }
            }
            if (c == '?' && stackModel != null) // pop-pop-type operations
            {
                stackModel.fireTableRowsDeleted(0, 1);
            }
            if (c == 'c') { // pop-call
                if (stackModel != null) stackModel.fireTableRowsDeleted(0, 0);
                if (callStackModel != null) callStackModel.fireTableRowsInserted(0, 0);
            }
            if (c == '$' && callStackModel != null) // return
            {
                callStackModel.fireTableRowsDeleted(0, 0);
            }
            if (c == '>') { // pop-pop-write
                if (stackModel != null) stackModel.fireTableRowsDeleted(0, 1);
                if (memoryModel != null && result >= 0) memoryModel.fireTableRowsInserted(result, result);
            }
            return true;
        } catch (VirtualMachineException ex) {
            lastException = ex;
            return false;
        }
    }

    public void updateTables() {
        if (stackModel != null) stackModel.fireTableDataChanged();
        if (memoryModel != null) memoryModel.fireTableDataChanged();
        if (callStackModel != null) callStackModel.fireTableDataChanged();
    }

    public void reset() {
        lastException = null;
        vm.reset();
        updateTables();
    }

    public void stepOver() {
        lastException = null;
        try {
            vm.stepOver();
        } catch (VirtualMachineException ex) {
            lastException = ex;
        }
        updateTables();
    }

    public void stepOut() {
        lastException = null;
        try {
            vm.stepOut();
        } catch (VirtualMachineException ex) {
            lastException = ex;
        }
        updateTables();
    }

    public void runTo(int ptr) {
        int codeptr = getCodePosition(ptr);
        lastException = null;
        try {
            vm.runTo(codeptr);
        } catch (VirtualMachineException ex) {
            lastException = ex;
        }
        updateTables();
    }

    public void goTo(int ptr) {
        int codeptr = getCodePosition(ptr);
        lastException = null;
        vm.setInstructionPointer(codeptr);
    }

    public void run() {
        lastException = null;
        try {
            vm.run();
        } catch (VirtualMachineException ex) {
            lastException = ex;
        }
        updateTables();
    }

    boolean stackGuessValid = false;
    int stackGuessPosition;

    private int stackGuessRecurse(char[] prog) {
        if (stackGuessPosition < 0 || stackGuessPosition >= prog.length) {
            stackGuessValid = false;
            return 0;
        }
        char c = prog[stackGuessPosition--];
        if (c >= '0' && c <= '9') return c - '0';
        if (c == '+') {
            int a = stackGuessRecurse(prog); // s0
            int b = stackGuessRecurse(prog); // s1
            if (stackGuessValid) {
                return b + a;
            } else {
                return 0;
            }
        }
        if (c == '-') {
            int a = stackGuessRecurse(prog); // s0
            int b = stackGuessRecurse(prog); // s1
            if (stackGuessValid) {
                return b - a;
            } else {
                return 0;
            }
        }
        if (c == '*') {
            int a = stackGuessRecurse(prog); // s0
            int b = stackGuessRecurse(prog); // s1
            if (stackGuessValid) {
                return b * a;
            } else {
                return 0;
            }
        }
        if (c == '/') {
            int a = stackGuessRecurse(prog); // s0
            int b = stackGuessRecurse(prog); // s1
            if (stackGuessValid) {
                return b / a;
            } else {
                return 0;
            }
        }
        stackGuessValid = false;
        return -1;
    }

    public int guessStackFromCode(int i) {
        stackGuessValid = false;
        char[] prog = vm.getProgram();
        stackGuessPosition = i - 1;
        stackGuessValid = true;
        int g = stackGuessRecurse(prog);
        if (stackGuessValid) return g;
        return 0;
    }

    public boolean isStackGuessValid() {
        return stackGuessValid;
    }

    public boolean canUndo = false;
    public static final String PROP_CANUNDO = "canUndo";

    /**
     * Get the value of canUndo
     *
     * @return the value of canUndo
     */
    public boolean isCanUndo() {
        return canUndo;
    }

    /**
     * Set the value of canUndo
     *
     * @param canUndo new value of canUndo
     */
    public void setCanUndo(boolean canUndo) {
        boolean oldCanUndo = this.canUndo;
        this.canUndo = canUndo;
        propertyChangeSupport.firePropertyChange(PROP_CANUNDO, oldCanUndo, canUndo);
    }

    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    /**
     * Add PropertyChangeListener.
     *
     * @param listener listener
     */
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    /**
     * Remove PropertyChangeListener.
     *
     * @param listener listener
     */
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    public void undo() {
        undoManager.undo();
        setCanUndo(undoManager.canUndo());
        setCanRedo(undoManager.canRedo());
    }

    public void redo() {
        undoManager.redo();
        setCanUndo(undoManager.canUndo());
        setCanRedo(undoManager.canRedo());
    }

    protected boolean canRedo = false;
    public static final String PROP_CANREDO = "canRedo";

    /**
     * Get the value of canRedo
     *
     * @return the value of canRedo
     */
    public boolean isCanRedo() {
        return canRedo;
    }

    /**
     * Set the value of canRedo
     *
     * @param canRedo new value of canRedo
     */
    public void setCanRedo(boolean canRedo) {
        boolean oldCanRedo = this.canRedo;
        this.canRedo = canRedo;
        propertyChangeSupport.firePropertyChange(PROP_CANREDO, oldCanRedo, canRedo);
    }

    /**
     * Writes this model to a stream.
     *
     * @param out stream to which the model will be written.
     * @throws java.io.IOException if model could not be written
     */
    public void writeObject(ObjectOutputStream out) throws IOException {
        try {
            out.writeObject(getText(0, getLength()));
        } catch (BadLocationException ex) {
            throw new IOException("Error writing program code: " + ex.getMessage());
        }
        vm.writeObject(out);
    }

    /**
     * Try to restore the model from a stream.
     *
     * @param in stream from which the model should be restored.
     * @throws java.io.IOException              if model could not be read
     * @throws java.lang.ClassNotFoundException if model class was not found for deserialization
     */
    public void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        String p = (String) in.readObject();
        vm.readObject(in);
        try {
            remove(0, getLength());
            insertString(0, p, null);
        } catch (BadLocationException ex) {
            throw new IOException("Error reading program code: " + ex.getMessage());
        }
        undoManager.discardAllEdits();
        updateProgramCode();
    }

    /**
     * Indicates if this model has been changed.
     *
     * @return true if this model has been changed since initialization or
     * the last call to <code>setSaved()</code>.
     */
    public boolean hasUnsavedChanges() {
        return changed;
    }

    /**
     * Set the model state to <i>unchanged</i>.
     */
    public void setSaved() {
        changed = false;
    }

    class StackTableModel extends AbstractTableModel {

        VirtualMachine vm;
        final String[] headings = {"#", "Value", "ASCII"};

        public StackTableModel(VirtualMachine vm) {
            this.vm = vm;
        }

        public int getColumnCount() {
            return 3;
        }

        @Override
        public String getColumnName(int column) {
            return headings[column];
        }

        public int getRowCount() {
            return vm.getStackSize();
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            try {
                if (columnIndex == 0) return "S" + rowIndex;
                if (columnIndex == 1) return vm.getStackElement(rowIndex);
                if (columnIndex == 2) {
                    int i = vm.getStackElement(rowIndex);
                    if (i < 0x20 || i > 0x7f) return '·';
                    return (char) i;
                }
            } catch (VirtualMachineException ignored) {
            }
            return "ERROR";
        }

    }

    class CallStackTableModel extends AbstractTableModel {

        VirtualMachine vm;
        final String[] headings = {"@", "Next Command"};

        public CallStackTableModel(VirtualMachine vm) {
            this.vm = vm;
        }

        public int getColumnCount() {
            return 2;
        }

        @Override
        public String getColumnName(int column) {
            return headings[column];
        }

        public int getRowCount() {
            return vm.getCallStackSize();
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return columnIndex == 0;
        }

        @Override
        public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
            if (columnIndex != 0) return;
            try {
                int i = Integer.parseInt(aValue.toString());
                vm.setCallStack(rowIndex, i);
                if (callStackModel != null) callStackModel.fireTableRowsUpdated(i, i);
            } catch (Exception ignored) {
            }
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            try {
                if (columnIndex == 0) return vm.getCallStack(rowIndex);
                if (columnIndex == 1) return vm.getProgram()[vm.getCallStack(rowIndex)];
            } catch (Exception ex) {
                if (columnIndex == 0) return "--";
                if (vm.getCallStack(rowIndex) == vm.getProgram().length) {
                    return "<end>";
                } else {
                    return "<invalid>";
                }
            }
            return "ERROR";
        }
    }

    class MemoryTableModel extends AbstractTableModel {

        VirtualMachine vm;
        final String[] headings = {"#", "Value", "ASCII"};

        public MemoryTableModel(VirtualMachine vm) {
            this.vm = vm;
        }

        public int getColumnCount() {
            return 3;
        }

        @Override
        public String getColumnName(int column) {
            return headings[column];
        }

        public int getRowCount() {
            return vm.getValidMemorySize();
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            try {
                if (columnIndex == 0) return vm.getValidMemoryIndex(rowIndex);
                if (columnIndex == 1) return vm.getMemory(vm.getValidMemoryIndex(rowIndex));
                if (columnIndex == 2) {
                    int i = vm.getMemory(vm.getValidMemoryIndex(rowIndex));
                    if (i < 0x20 || i > 0x7f) return '·';
                    return (char) i;
                }
            } catch (VirtualMachineException ignored) {
            }
            return "ERROR";
        }

    }
}
