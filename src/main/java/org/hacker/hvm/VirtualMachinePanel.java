/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * VirtualMachinePanel.java
 *
 * Created on 15.11.2008, 23:35:21
 */
package org.hacker.hvm;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import javax.swing.text.BadLocationException;
import java.awt.*;

public class VirtualMachinePanel extends javax.swing.JPanel {

    VirtualMachineModel vm;
    VirtualMachineFrameView parent;
    private javax.swing.JTable callStackTable;
    private javax.swing.JLabel cyclesLabel;
    private javax.swing.JLabel errorLabel;
    private javax.swing.JTable memoryTable;
    private javax.swing.JTextField outputText;
    private javax.swing.JLabel positionLabel;
    private javax.swing.JTextArea programArea;
    private javax.swing.JSplitPane splitPane;
    private javax.swing.JTable stackTable;

    /** Creates new form VirtualMachinePanel */
    public VirtualMachinePanel(VirtualMachineFrameView parent) {
        vm = new VirtualMachineModel();
        this.parent = parent;
        initComponents();
        updateCaretPosition();
        updateOutput();
        requestFocus();
        TableCellRenderer addrRenderer = (table, value, isSelected, hasFocus, row, column) -> {
            JTextField f = new JTextField(value.toString());
            f.setFont(new java.awt.Font("Monospaced", Font.PLAIN, 13));
            f.setHorizontalAlignment(JTextField.RIGHT);
            f.setEditable(table.getModel().isCellEditable(row, column));
            if (isSelected) {
                f.setBackground(table.getSelectionBackground());
                f.setForeground(table.getSelectionForeground());
            }
            f.setBorder(null);
            return f;
        };
        stackTable.getColumnModel().getColumn(0).setMaxWidth(50);
        stackTable.getColumnModel().getColumn(0).setPreferredWidth(25);
        stackTable.getColumnModel().getColumn(0).setCellRenderer(addrRenderer);
        memoryTable.getColumnModel().getColumn(0).setMaxWidth(50);
        memoryTable.getColumnModel().getColumn(0).setPreferredWidth(25);
        memoryTable.getColumnModel().getColumn(0).setCellRenderer(addrRenderer);
        callStackTable.getColumnModel().getColumn(0).setMaxWidth(50);
        callStackTable.getColumnModel().getColumn(0).setPreferredWidth(25);
        callStackTable.getColumnModel().getColumn(0).setCellRenderer(addrRenderer);
        // avoid F8 resizing split pane
        splitPane.getActionMap().getParent().remove("startResize");
        EventQueue.invokeLater(() -> splitPane.setDividerLocation(0.5));
    }

    @Override
    public void requestFocus() {
        super.requestFocus();
        EventQueue.invokeLater(() -> programArea.requestFocus());
    }

    public VirtualMachineModel getModel() {
        return vm;
    }

    public void updateOutput() {
        cyclesLabel.setText(vm.getCycles() + " cycles");
        errorLabel.setText(vm.getStatusMessage());
        String output = vm.getOutput();
        if (output == null || output.isEmpty()) {
            outputText.setForeground(SystemColor.textInactiveText);
            outputText.setText("Output");
        } else {
            outputText.setForeground(SystemColor.textText);
            outputText.setText(output);
        }
    }

    public void updateCaretPosition() {
        int caretPosition = programArea.getCaretPosition();
        int ip = vm.getCodePosition(caretPosition),
                instr = vm.getInstructionPointer();
        int x, y;
        try {
            y = programArea.getLineOfOffset(caretPosition) + 1;
            x = caretPosition - programArea.getLineStartOffset(y - 1);
        } catch (BadLocationException ex) {
            return;
        }
        if (vm.isDocumentPositionInComment(caretPosition)) {
            positionLabel.setText(y + ":" + x);
        } else {
            positionLabel.setText(y + ":" + x + " (Instruction " + ip + ")");
        }
        char c = vm.getCommandAt(ip);
        if (c == 'c') { // highlight area targeted by this call
            int from = vm.guessStackFromCode(ip), to;
            if (vm.isStackGuessValid()) {
                char[] p = vm.getProgram();
                for (to = from; to < p.length; to++) {
                    if (p[to] == '$') {
                        to++;
                        break;
                    }
                }
                ((JProgramTextArea) programArea).setHighlights(from, to, instr);
                return;
            }
        } else if (c == '?' || c == 'g') { // highlight area targeted by this jump
            int to = vm.guessStackFromCode(ip);
            if (vm.isStackGuessValid()) {
                to += ip + 1;
                ((JProgramTextArea) programArea).setHighlights(to, to + 1, instr);
                return;
            }
        }
        ((JProgramTextArea) programArea).setHighlights(0, -1, instr);
    }

    public int getCaretPosition() {
        return programArea.getCaretPosition();
    }

    public void undo() {
        vm.undo();
    }

    public void redo() {
        vm.redo();
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     */
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        splitPane = new javax.swing.JSplitPane();
        javax.swing.JPanel topPanel = new javax.swing.JPanel();
        javax.swing.JScrollPane programScrollPane = new javax.swing.JScrollPane();
        programArea = new JProgramTextArea();
        javax.swing.JPanel programPanel = new javax.swing.JPanel();
        positionLabel = new javax.swing.JLabel();
        javax.swing.JSeparator jSeparator1 = new javax.swing.JSeparator();
        cyclesLabel = new javax.swing.JLabel();
        javax.swing.JSeparator jSeparator2 = new javax.swing.JSeparator();
        errorLabel = new javax.swing.JLabel();
        javax.swing.JPanel bottomPanel = new javax.swing.JPanel();
        outputText = new javax.swing.JTextField();
        javax.swing.JPanel dataPanel = new javax.swing.JPanel();
        javax.swing.JLabel stackLabel = new javax.swing.JLabel();
        javax.swing.JScrollPane stackScrollPane = new javax.swing.JScrollPane();
        stackTable = new javax.swing.JTable();
        javax.swing.JLabel memoryLabel = new javax.swing.JLabel();
        javax.swing.JScrollPane memoryScrollPane = new javax.swing.JScrollPane();
        memoryTable = new javax.swing.JTable();
        javax.swing.JLabel callStackLabel = new javax.swing.JLabel();
        javax.swing.JScrollPane callStackScrollPane = new javax.swing.JScrollPane();
        callStackTable = new javax.swing.JTable();

        setName("Form");
        setLayout(new java.awt.BorderLayout());

        splitPane.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        splitPane.setDividerLocation(150);
        splitPane.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        splitPane.setName("splitPane");
        splitPane.setOpaque(false);

        topPanel.setName("topPanel");
        topPanel.setOpaque(false);
        topPanel.setLayout(new java.awt.BorderLayout());

        programScrollPane.setName("programScrollPane");

        programArea.setColumns(60);
        programArea.setDocument(vm);
        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(org.hacker.hvm.VirtualMachineApp.class).getContext()
                .getResourceMap(VirtualMachinePanel.class);
        programArea.setText(resourceMap.getString("programArea.text"));
        programArea.setName("programArea");
        programArea.addCaretListener(this::programAreaCaretUpdate);
        programScrollPane.setViewportView(programArea);

        topPanel.add(programScrollPane, java.awt.BorderLayout.CENTER);

        programPanel.setName("programPanel");
        programPanel.setOpaque(false);
        programPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 2));

        positionLabel.setText(resourceMap.getString("positionLabel.text"));
        positionLabel.setName("positionLabel");
        programPanel.add(positionLabel);

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jSeparator1.setName("jSeparator1");
        jSeparator1.setPreferredSize(new java.awt.Dimension(2, 20));
        programPanel.add(jSeparator1);

        cyclesLabel.setText(resourceMap.getString("cyclesLabel.text"));
        cyclesLabel.setName("cyclesLabel");
        programPanel.add(cyclesLabel);

        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jSeparator2.setName("jSeparator2");
        jSeparator2.setPreferredSize(new java.awt.Dimension(2, 20));
        programPanel.add(jSeparator2);

        errorLabel.setText(resourceMap.getString("errorLabel.text"));
        errorLabel.setName("errorLabel");
        programPanel.add(errorLabel);

        topPanel.add(programPanel, java.awt.BorderLayout.SOUTH);

        splitPane.setLeftComponent(topPanel);

        bottomPanel.setName("bottomPanel");
        bottomPanel.setOpaque(false);

        outputText.setEditable(false);
        outputText.setForeground(resourceMap.getColor("outputText.foreground"));
        outputText.setText(resourceMap.getString("outputText.text"));
        outputText.setName("outputText");

        dataPanel.setName("dataPanel");
        dataPanel.setOpaque(false);
        dataPanel.setLayout(new java.awt.GridBagLayout());

        stackLabel.setText(resourceMap.getString("stackLabel.text"));
        stackLabel.setName("stackLabel");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        dataPanel.add(stackLabel, gridBagConstraints);

        stackScrollPane.setName("stackScrollPane");

        stackTable.setModel(vm.getStackTableModel());
        stackTable.setName("stackTable");
        stackScrollPane.setViewportView(stackTable);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        dataPanel.add(stackScrollPane, gridBagConstraints);

        memoryLabel.setText(resourceMap.getString("memoryLabel.text"));
        memoryLabel.setName("memoryLabel");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        dataPanel.add(memoryLabel, gridBagConstraints);

        memoryScrollPane.setName("memoryScrollPane");

        memoryTable.setModel(vm.getMemoryTableModel());
        memoryTable.setName("memoryTable");
        memoryScrollPane.setViewportView(memoryTable);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        dataPanel.add(memoryScrollPane, gridBagConstraints);

        callStackLabel.setText(resourceMap.getString("callStackLabel.text"));
        callStackLabel.setName("callStackLabel");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        dataPanel.add(callStackLabel, gridBagConstraints);

        callStackScrollPane.setName("callStackScrollPane");

        callStackTable.setModel(vm.getCallStackTableModel());
        callStackTable.setName("callStackTable");
        callStackScrollPane.setViewportView(callStackTable);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        dataPanel.add(callStackScrollPane, gridBagConstraints);

        javax.swing.GroupLayout bottomPanelLayout = new javax.swing.GroupLayout(bottomPanel);
        bottomPanel.setLayout(bottomPanelLayout);
        bottomPanelLayout.setHorizontalGroup(
                bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(bottomPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(outputText, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 498,
                                                Short.MAX_VALUE)
                                        .addComponent(dataPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 498,
                                                Short.MAX_VALUE))
                                .addContainerGap())
        );
        bottomPanelLayout.setVerticalGroup(
                bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(bottomPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(outputText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                                        javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(dataPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE)
                                .addContainerGap())
        );

        splitPane.setRightComponent(bottomPanel);

        add(splitPane, java.awt.BorderLayout.CENTER);
    }

    private void programAreaCaretUpdate(javax.swing.event.CaretEvent evt) {
        EventQueue.invokeLater(this::updateCaretPosition);

    }
}
