/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hacker.hvm;

import java.io.IOException;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.Scanner;

// TODO: add unit tests

public class VirtualMachine implements Serializable {

    final byte MAJOR_VERSION = 1;
    final byte MINOR_VERSION = 0;
    final byte PATCHLEVEL = 0;
    int memorySize = 16384;
    int stackSize = 65000;
    int callStackSize = 65000;
    int cycleLimit = 50000;
    char[] prog;
    int instructionptr;
    int[] mem, stack, callstack;
    int[] validmemaddr, validmemorder;
    int validmemsize;
    int stackptr, workstackptr, callstackptr, workcallstackptr;
    VirtualMachineException lastException;
    StringBuffer output;
    int cycles;

    public VirtualMachine() {
        initialize("", "");
    }

    public VirtualMachine(String p, String m) {
        initialize(p, m);
    }

    void initialize(String program, String memory) {
        prog = program.toCharArray();
        instructionptr = 0;
        cycles = 0;
        mem = new int[memorySize];
        validmemaddr = new int[memorySize];
        validmemorder = new int[memorySize];
        initializeMemoryFromString(memory);
        stack = new int[stackSize];
        stackptr = 0;
        workstackptr = 0;
        callstack = new int[callStackSize];
        callstackptr = 0;
        workcallstackptr = 0;
        output = new StringBuffer(1000);
        lastException = null;
    }

    public void initializeMemoryFromString(String m) {
        for (int i = 0; i < validmemaddr.length; i++) {
            validmemaddr[i] = -1;
            validmemorder[i] = -1;
        }
        validmemsize = 0;
        Scanner in = new Scanner(m);
        in.useDelimiter(" *, *");
        while (in.hasNextInt() && validmemsize <= memorySize) {
            mem[validmemsize] = in.nextInt();
            validmemorder[validmemsize] = validmemsize;
            validmemaddr[validmemsize] = validmemsize;
            validmemsize++;
        }
    }

    char[] getProgram() {
        return prog;
    }

    void setProgram(String p) {
        prog = p.toCharArray();
    }

    char getCommandAt(int i) {
        if (i >= 0 && i < prog.length) {
            return prog[i];
        } else {
            return (char) 0;
        }
    }

    char getNextCommand() {
        if (instructionptr >= 0 && instructionptr < prog.length) {
            return prog[instructionptr];
        } else {
            return (char) 0;
        }
    }

    public int getInstructionPointer() {
        return instructionptr;
    }

    public void setInstructionPointer(int i) {
        instructionptr = i;
    }

    /**
     * Set memory cell <code>addr</code> to value <code>val</code>.
     *
     * @param addr Address of the memory cell.
     * @param val  New or changed value of the memory cell.
     * @return -1 if memory cell was changed; if the memory cell was initialized,
     * i.e. written for the first time, its number in the sequence of initialized
     * memory cells is returned.
     * @throws org.hacker.hvm.VirtualMachine.VirtualMachineException if any error
     */
    int setMemory(int addr, int val) throws VirtualMachineException {
        if (addr < 0 || addr >= memorySize) {
            throw new VirtualMachineException("memory write access to " + addr + " out of range", this);
        }
        mem[addr] = val;
        int o = validmemorder[addr];
        if (o != -1) { // memory was not valid before -> valid memory cell order does not change
            return -1;
        } else { // insert new memory cell in the list of valid memory cells
            int i = 0, inserted;
            while (validmemaddr[i] < addr && validmemaddr[i] != -1) i++;
            for (inserted = i; i < validmemsize; i++) {
                validmemorder[validmemaddr[i]]++;
                validmemaddr[i + 1] = validmemaddr[i];
            }
            validmemaddr[inserted] = addr;
            validmemorder[addr] = inserted;
            validmemsize++;
            return inserted;
        }
    }

    int getMemory(int addr) throws VirtualMachineException {
        if (addr < 0 || addr > memorySize) {
            throw new VirtualMachineException("invalid memory read access to address " + addr, this);
        }
        if (validmemorder[addr] == -1) return 0;
        return mem[addr];
    }

    void loadAsciiCharsIntoMemory(String s) throws VirtualMachineException {
        validmemsize = s.length() + 1;
        if (validmemsize > memorySize) validmemsize = memorySize;
        int i;
        for (i = 0; i < s.length() && i < validmemsize; i++) {
            mem[i] = s.charAt(i);
            validmemorder[i] = i;
            validmemaddr[i] = i;
        }
        if (s.length() >= memorySize) {
            throw new VirtualMachineException("string length exceeds memory size", this);
        }
        mem[i] = 0;
        validmemorder[i] = i;
        validmemaddr[i] = i;
        i++;
        while (i < memorySize) {
            validmemorder[i] = -1;
            validmemaddr[i] = -1;
            i++;
        }
    }

    public int getMemorySize() {
        return memorySize;
    }

    public void setMemorySize(int m) {
        if (memorySize == m) return;
        int[] nm = new int[m];
        int[] nvma = new int[m];
        int[] nvmo = new int[m];
        int nvms = 0;
        for (int i = 0; i < m && i < memorySize; i++) {
            nvma[i] = -1;
            nvmo[i] = -1;
            if (validmemorder[i] != -1) {
                nm[i] = mem[i];
                nvma[nvms] = i;
                nvmo[i] = nvms;
                nvms++;
            }
        }
        validmemsize = nvms;
        validmemaddr = nvma;
        validmemorder = nvmo;
        memorySize = m;
    }

    public int getValidMemorySize() {
        return validmemsize;
    }

    public int getValidMemoryIndex(int i) {
        return validmemaddr[i];
    }

    public int getStackSize() {
        return stackptr;
    }

    public int getMaxStackSize() {
        return stackSize;
    }

    public void setMaxStackSize(int s) {
        if (stackSize == s) return;
        int[] ns = new int[s];
        for (int i = 0; i < s && i < stackSize; i++) ns[i] = stack[i];
        if (stackptr > s) stackptr = s;
        stackSize = s;
    }

    public int getMaxCallStackSize() {
        return callStackSize;
    }

    public void setMaxCallStackSize(int s) {
        if (callStackSize == s) return;
        int[] ns = new int[s];
        for (int i = 0; i < s && i < callStackSize; i++) ns[i] = callstack[i];
        if (callstackptr > s) callstackptr = s;
        callStackSize = s;
    }

    int getStackElement(int i) throws VirtualMachineException {
        if (i < 0 || i >= stackptr) {
            throw new VirtualMachineException("invalid stack read access to S" + i, this);
        }
        return stack[stackptr - i - 1];
    }

    void setStackElement(int i, int val) throws VirtualMachineException {
        if (i < 0 || i >= stackptr) {
            throw new VirtualMachineException("invalid stack write access to S" + i, this);
        }
        stack[stackptr - i - 1] = val;
    }

    int getCallStackSize() {
        return callstackptr;
    }

    int getCallStack(int i) {
        if (i < 0 || i > callstackptr - 1) return -1;
        return callstack[callstackptr - i - 1];
    }

    void setCallStack(int i, int value) {
        callstack[callstackptr - i - 1] = value;
    }

    public int getCycles() {
        return cycles;
    }

    public void setCycles(int cycles) {
        this.cycles = cycles;
    }

    public int getCycleLimit() {
        return cycleLimit;
    }

    public void setCycleLimit(int CycleLimit) {
        this.cycleLimit = CycleLimit;
    }

    boolean print(String s) {
        output.append(s);
        return true;
    }

    boolean print(char c) {
        output.append(c);
        return true;
    }

    public String getOutput() {
        return output.toString();
    }

    int pop() throws VirtualMachineException {
        if (workstackptr == 0) {
            throw new VirtualMachineException("stack underflow: cannot pop from empty stack", this);
        }
        return stack[--workstackptr];
    }

    int peek() throws VirtualMachineException {
        if (workstackptr == 0) {
            throw new VirtualMachineException("stack underflow: cannot peek on empty stack", this);
        }
        return stack[workstackptr - 1];
    }

    void push(int p) throws VirtualMachineException {
        if (workstackptr == stackSize) {
            throw new VirtualMachineException("stack overflow", this);
        }
        stack[workstackptr++] = p;
    }

    void call(int p) throws VirtualMachineException {
        if (workcallstackptr == callStackSize) {
            throw new VirtualMachineException("call stack overflow: infinite recursion?", this);
        }
        callstack[workcallstackptr++] = instructionptr;
        instructionptr = p;
    }

    void assertStackSize(int s) throws VirtualMachineException {
        if (stackptr < s) throw new VirtualMachineException("stack underflow: need at least " + s + " stack elements", this);
    }

    int step() throws VirtualMachineException {
        if (cycles == cycleLimit) {
            throw new VirtualMachineException("execution limit of " + cycleLimit + " cycles reached", this);
        }
        if (instructionptr < 0 || instructionptr > prog.length) {
            throw new VirtualMachineException("invalid instruction pointer", this);
        }
        if (instructionptr == prog.length) {
            throw new VirtualMachineException("end of program reached", this);
        }
        char command = prog[instructionptr];
        workstackptr = stackptr;
        workcallstackptr = callstackptr;
        int result = 0;
        if (command >= '0' && command <= '9') {
            push(command - '0');
        } else {
            int s0, s1;
            switch (command) {
                case ' ':
                case '\n':
                    break;
                case 'p':
                    print(Integer.toString(pop()));
                    break;
                case 'P':
                    print((char) (pop() & 0x7f));
                    break;
                case '+':
                    assertStackSize(2);
                    push(pop() + pop());
                    break;
                case '-':
                    assertStackSize(2);
                    s0 = pop();
                    s1 = pop();
                    push(s1 - s0);
                    break;
                case '*':
                    assertStackSize(2);
                    push(pop() * pop());
                    break;
                case '/':
                    assertStackSize(2);
                    s0 = pop();
                    s1 = pop();
                    if (s0 == 0) throw new VirtualMachineException("division by zero", this);
                    push((int) Math.floor((double) s1 / s0));
                    break;
                case ':':
                    assertStackSize(2);
                    s0 = pop();
                    s1 = pop();
                    push(Integer.signum(s1 - s0));
                    break;
                case 'g':
                    instructionptr += pop();
                    break;
                case '?':
                    assertStackSize(2);
                    s0 = pop();
                    s1 = pop();
                    if (s1 == 0) instructionptr += s0;
                    break;
                case 'c':
                    call(pop());
                    instructionptr--; // will be increased again after the switch block
                    break;
                case '$':
                    if (workcallstackptr == 0) {
                        throw new VirtualMachineException("call stack underflow", this);
                    }
                    instructionptr = callstack[--workcallstackptr];
                    break;
                case '<':
                    push(getMemory(pop()));
                    break;
                case '>':
                    assertStackSize(2);
                    int address = pop(),
                            data = pop();
                    result = setMemory(address, data);
                    break;
                case '^':
                    s0 = pop();
                    push(getStackElement(s0 + 1));
                    break;
                case 'v':
                    s0 = pop();
                    if (s0 < 0 || s0 >= workstackptr) {
                        throw new VirtualMachineException("invalid stack remove access to S" + s0, this);
                    }
                    int val = stack[workstackptr - s0 - 1];
                    for (int j = workstackptr - s0; j < workstackptr; j++) {
                        stack[j - 1] = stack[j];
                    }
                    workstackptr--;
                    push(val);
                    break;
                case 'd':
                    if (workstackptr == 0) {
                        throw new VirtualMachineException("stack underflow: cannot drop from empty stack", this);
                    }
                    workstackptr--;
                    break;
                case '!':
                    throw new VirtualMachineException("program terminated by command '!'", this);
                default:
                    throw new VirtualMachineException("unknown command '" + command + "'", this);
            }
        }
        stackptr = workstackptr;
        callstackptr = workcallstackptr;
        instructionptr++;
        cycles++;
        return result;

    }

    public String getState() {
        StringBuilder b = new StringBuilder(100);
        b.append("@").append(instructionptr);
        if (instructionptr == 0 || instructionptr == prog.length) {
            b.append("  ");
        } else if (instructionptr < 0 || instructionptr > prog.length) {
            b.append(" invalid instruction ptr");
        } else {
            b.append(" ").append(prog[instructionptr - 1]);
        }
        b.append(" [");
        for (int i = 0; i < stackptr; i++) {
            b.append(stack[i]);
            if (i < stackptr - 1) b.append(" ");
        }
        b.append("]");
        return b.toString();
    }

    public void run() throws VirtualMachineException {
        //noinspection InfiniteLoopStatement
        while (true) step();
    }

    public void runTo(int ptr) throws VirtualMachineException {
        while (instructionptr != ptr) step();
    }

    public void stepOut() throws VirtualMachineException {
        do step(); while (instructionptr < prog.length && prog[instructionptr] == '$');
    }

    public void stepOver() throws VirtualMachineException {
        if (instructionptr < prog.length) {
            if (prog[instructionptr] == 'c') {
                int to = instructionptr + 1, css = callstackptr;
                while (!(instructionptr == to && callstackptr == css)) step();
            } else {
                step();
            }
        }
    }

    public void reset() {
        instructionptr = 0;
        stackptr = 0;
        callstackptr = 0;
        cycles = 0;
        output = new StringBuffer(1000);
    }

    public void writeObject(java.io.ObjectOutputStream out) throws IOException {
        out.write("HVM".getBytes());
        out.writeByte(MAJOR_VERSION);
        out.writeByte(MINOR_VERSION);
        out.writeByte(PATCHLEVEL);
        out.writeObject(new String(prog));
        out.writeInt(instructionptr);
        out.writeInt(cycles);
        out.writeInt(cycleLimit);
        out.writeInt(stackSize);
        out.writeInt(stackptr);
        for (int i = 0; i < stackptr; i++) {
            out.writeInt(stack[i]);
        }
        out.writeInt(memorySize);
        out.writeInt(validmemsize);
        for (int i = 0; i < validmemsize; i++) {
            out.writeInt(validmemaddr[i]);
            out.writeInt(mem[i]);
        }
        out.writeInt(callStackSize);
        out.writeInt(callstackptr);
        for (int i = 0; i < callstackptr; i++) {
            out.writeInt(callstack[i]);
        }
    }

    public void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
        byte[] buf = new byte[3];
        if (in.read(buf) != 3 || !new String(buf).equals("HVM")) {
            throw new IOException("Invalid file format");
        }
        int majorVersion = in.readByte(), minorVersion = in.readByte(), patchlevel = in.readByte();
        if (majorVersion > 1 && minorVersion > 0 && patchlevel > 0) {
            throw new IOException("Invalid file version " + majorVersion + "." + minorVersion + "." + patchlevel);
        }
        String p = (String) in.readObject();
        prog = p.toCharArray();
        instructionptr = in.readInt();
        cycles = in.readInt();
        cycleLimit = in.readInt();
        int s = in.readInt();
        if (s != stackSize) {
            stack = new int[s];
            stackSize = s;
        }
        stackptr = in.readInt();
        for (int i = 0; i < stackptr; i++) stack[i] = in.readInt();
        s = in.readInt();
        if (memorySize != s) {
            mem = new int[s];
            validmemaddr = new int[s];
            validmemorder = new int[s];
            memorySize = s;
        }
        for (int i = 0; i < memorySize; i++) {
            validmemaddr[i] = -1;
            validmemorder[i] = -1;
        }
        int v = in.readInt();
        for (int i = 0; i < v; i++) {
            try {
                setMemory(in.readInt(), in.readInt());
            } catch (VirtualMachineException ex) {
                throw new IOException("Could not restore memory: " + ex.getMessage());
            }

        }
        s = in.readInt();
        if (s != callStackSize) {
            callstack = new int[s];
            callStackSize = s;
        }
        callstackptr = in.readInt();
        for (int i = 0; i < callstackptr; i++) callstack[i] = in.readInt();
    }

    public void readObjectNoData() throws ObjectStreamException {
        initialize("", "");
    }

    public class VirtualMachineException extends Exception {
        private final int ip;

        public VirtualMachineException(String message, VirtualMachine vm) {
            super(message);
            ip = instructionptr;
            vm.lastException = this;
        }

        public int getInstructionPointer() {
            return ip;
        }
    }
}
