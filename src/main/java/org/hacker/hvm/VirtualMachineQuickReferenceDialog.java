/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * VirtualMachineQuickReferenceDialog.java
 *
 * Created on 18.11.2008, 01:05:04
 */

package org.hacker.hvm;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

public class VirtualMachineQuickReferenceDialog extends javax.swing.JDialog {

    private javax.swing.JTable helpTable;

    /** Creates new form VirtualMachineQuickReferenceDialog */
    public VirtualMachineQuickReferenceDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        TableCellRenderer codeRenderer = (table, value, isSelected, hasFocus, row, column) -> {
            JTextField f = new JTextField(value.toString());
            f.setFont(new java.awt.Font("Monospaced", Font.PLAIN, 13));
            f.setHorizontalAlignment(JTextField.CENTER);
            f.setEditable(table.getModel().isCellEditable(row, column));
            if (isSelected) {
                f.setBackground(table.getSelectionBackground());
                f.setForeground(table.getSelectionForeground());
            }
            f.setBorder(null);
            return f;
        };
        helpTable.getColumnModel().getColumn(0).setMaxWidth(80);
        helpTable.getColumnModel().getColumn(0).setPreferredWidth(40);
        helpTable.getColumnModel().getColumn(0).setCellRenderer(codeRenderer);

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(() -> {
            VirtualMachineQuickReferenceDialog dialog = new VirtualMachineQuickReferenceDialog(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     */
    private void initComponents() {

        // Variables declaration - do not modify
        javax.swing.JScrollPane helpScrollPane = new javax.swing.JScrollPane();
        helpTable = new javax.swing.JTable();
        javax.swing.JLabel jLabel1 = new javax.swing.JLabel();
        javax.swing.JScrollPane jScrollPane1 = new javax.swing.JScrollPane();
        javax.swing.JTextArea jTextArea1 = new javax.swing.JTextArea();
        javax.swing.JLabel jLabel2 = new javax.swing.JLabel();
        javax.swing.JLabel jLabel3 = new javax.swing.JLabel();
        javax.swing.JLabel jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(org.hacker.hvm.VirtualMachineApp.class).getContext()
                .getResourceMap(VirtualMachineQuickReferenceDialog.class);
        setTitle(resourceMap.getString("Quick Reference.title"));
        setName("Quick Reference");

        helpScrollPane.setName("helpScrollPane");

        helpTable.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{
                        {"# ...", "comment"},
                        {"p", "print S0 as integer"},
                        {"P", "print S0 as ASCII"},
                        {"0..9", "push 0, ..., 9 on stack"},
                        {"+-*/", "push S1[+-*/]S0"},
                        {":", "push sgn(S1-S0)"},
                        {"g", "add S0 to program counter"},
                        {"?", "add S0 to program counter if S1==0"},
                        {"c", "call program position S0"},
                        {"$", "return from previous call"},
                        {"<", "push the value of memory cell S0"},
                        {">", "store S1 into memory cell S0"},
                        {"^", "push S[S0+1] on stack (0^ = duplicate S0)"},
                        {"v", "move S[S0+1] on top (1v swaps S0 and S1)"},
                        {"d", "drop S0"},
                        {"!", "terminate the program"}
                },
                new String[]{
                        "Cmd", "Help"
                }
        ) {
            final Class<?>[] types = new Class[]{
                    java.lang.String.class, java.lang.String.class
            };

            public Class<?> getColumnClass(int columnIndex) {
                return types[columnIndex];
            }
        });
        helpTable.setName("helpTable");
        helpScrollPane.setViewportView(helpTable);
        helpTable.getColumnModel().getColumn(0).setHeaderValue(resourceMap.getString("helpTable.columnModel.title0"));
        helpTable.getColumnModel().getColumn(1).setHeaderValue(resourceMap.getString("helpTable.columnModel.title1"));

        jLabel1.setFont(jLabel1.getFont().deriveFont(jLabel1.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel1.setText(resourceMap.getString("jLabel1.text"));
        jLabel1.setName("jLabel1");

        jScrollPane1.setName("jScrollPane1");

        jTextArea1.setColumns(20);
        jTextArea1.setEditable(false);
        jTextArea1.setRows(2);
        jTextArea1.setText(resourceMap.getString("jTextArea1.text"));
        jTextArea1.setName("jTextArea1");
        jScrollPane1.setViewportView(jTextArea1);

        jLabel2.setFont(jLabel2.getFont().deriveFont(jLabel2.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel2.setText(resourceMap.getString("jLabel2.text"));
        jLabel2.setName("jLabel2");

        jLabel3.setFont(jLabel3.getFont());
        jLabel3.setText(resourceMap.getString("jLabel3.text"));
        jLabel3.setName("jLabel3");

        jLabel4.setFont(jLabel4.getFont());
        jLabel4.setText(resourceMap.getString("jLabel4.text"));
        jLabel4.setName("jLabel4");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 554, Short.MAX_VALUE)
                                        .addComponent(jLabel1)
                                        .addComponent(jLabel3)
                                        .addComponent(jLabel4)
                                        .addComponent(jLabel2)
                                        .addComponent(helpScrollPane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 554,
                                                Short.MAX_VALUE))
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                                        javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel4)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(helpScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 330, Short.MAX_VALUE)
                                .addContainerGap())
        );

        pack();
    }

}
