/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.hacker.hvm;

import javax.swing.*;
import java.awt.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

// TODO: add close button

public class JRenameableTab extends JTextField {
    JTabbedPane myPane;
    JComponent myComponent;
    JRenameableTab me = this;
    boolean editing = false;

    public JRenameableTab(JTabbedPane t, JComponent c) {
        myPane = t;
        myComponent = c;
        this.setEditable(false);
        this.setOpaque(false);
        this.setBorder(BorderFactory.createEmptyBorder());
        this.setText(t.getTitleAt(t.indexOfComponent(c)));
        this.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    editing = true;
                    setEditable(true);
                    setOpaque(true);
                    selectAll();
                } else {
                    myPane.setSelectedComponent(myComponent);
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                if (editing) return;
                // allow tab rollover effect by redispatching the mouse event
                MouseEvent n = SwingUtilities.convertMouseEvent(me, e, myPane);
                myPane.dispatchEvent(n);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                if (editing) return;
                // allow tab rollover effect by redispatching the mouse event
                MouseEvent n = SwingUtilities.convertMouseEvent(me, e, myPane);
                myPane.dispatchEvent(n);
            }

        });

        this.addActionListener(e -> {
            setEditable(false);
            setOpaque(false);
            editing = false;
            myPane.setTitleAt(myPane.indexOfComponent(myComponent), getText());
        });
        this.addFocusListener(new FocusAdapter() {

            @Override
            public void focusLost(FocusEvent e) {
                setEditable(false);
                setOpaque(false);
                editing = false;
                int i = myPane.indexOfComponent(myComponent);
                if (i < 0) return; // happens e.g. when closing a focused tab
                myPane.setTitleAt(i, getText());
            }

        });
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension orig = super.getPreferredSize();
        orig.width++;
        return orig;
    }
}
