# Hacker Virtual Machine IDE

Compile and run using Maven:

```shell
mvn clean install
java -jar target/hvm-java-ide.jar
```

Apart from that, have fun hacking, but keep in mind that this code is almost 14 years old.
Today I would have used [Svelte](https://svelte.dev) or [elm](https://elm-lang.org/), I think.
